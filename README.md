**Trabajo Nro 2 GCC - Sistema de Login**
Alumna: María Andrea Benítez Barrios

**Requerimientos**
*  dj-database-url==0.5.0
*  Django==2.2.1
*  django-heroku==0.3.1
*  gunicorn==19.9.0
*  psycopg2==2.8.2
*  pytz==2019.1
*  sqlparse==0.3.0
*  whitenoise==4.1.2

**Desplegado en 3 stages**
1.  stage: build correspondiente a Desarrollo
2.  stage: test correspondiente a la Homologación
3.  stage: deploy

**Desarrollo y producción son deployados en Heroku**
*  Desarrollo se encuentra en: https://gccv0101.herokuapp.com/
*  Producción se encuentra en: https://gccv0102.herokuapp.com/

**Los test realizados**
*  Un get inicial de la página principal
*  Un test para probar el login con un super usuario creado
*  Un test fallido para probar que no loguea a un usuario no creado
*  Un test para probar el logout del sistema

**Usuarios ya creados para la prueba del login en desarrollo y producción**
*  Username= ellen, password = Gcc2019.
*  Username = test, password = test2019.

**La configuración para la carga de usuarios fue realizado mediante el GUI de heroku**

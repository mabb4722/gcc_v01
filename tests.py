from django.contrib.auth.models import AnonymousUser, User
from django.test import RequestFactory, TestCase
from django.test.client import Client


class SimpleTest(TestCase):
    def setUp(self):
        self.user = User.objects.create_superuser (username='test1', email='mabb4722@gmail.com', password='Gcc2019.')

    def test_1(self):
        print('Get inicial')
        # Create an instance of a GET request.
        request = self.client.get('/')
        self.assertEqual(request.status_code, 200)

    def test_login(self):
        print ('Test Login')
        client = Client ()
        self.assertEqual (client.login (username='test1', password='Gcc2019.'), True)

    def test_logout(self):
        print('Test Logout')
        # Log in
        self.client.login (username='test1', password="Gcc2019.")
        # Log out
        self.client.logout()
        response = self.client.get ('/accounts/login/')
        self.assertEquals (response.status_code, 200)

    def test_login_fallo(self):
        print('Tes login fallo')
        client = Client ()
        self.assertEqual (client.login (username='test2', password='null'), False)